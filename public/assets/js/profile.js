// console.log("Hello from JS!")
// get the value of the access token inside the local storage and place it inside a new variable

let token = localStorage.getItem("token");
// console.log(token)

let lalagyan = document.querySelector('#profileContainer');


// our goal here is to display the information about the user details
// send a request to the back end project

fetch("https://wonderful-sky-1993.herokuapp.com/api/users/details", {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
}).then(res => res.json())
.then(jsonData => {
	if (jsonData) {
	
	let subjectName;
	let userSubjectPromises = jsonData.enrollments.map(subject => {
		return fetch(`https://wonderful-sky-1993.herokuapp.com/api/courses/${subject.courseId}`).then(res => res.json()).then(subjectData => {
			// console.log(subjectData)
			subjectName = subjectData.name
		
		return (
				// analyze all the data we already have and how am I going to use that information to solve the task.
				`
					<tr>
						<td>${subjectData.name}</td>
						<td>${subjectData.description}</td>
						<td>${subject.enrolledOn}</td>
						<td>${subject.status}</td>
					</tr>
				`
			)
		})
	});
		return Promise.all(userSubjectPromises).then(results => {
        	const newHtml = results.join("");
        	document.getElementById("profileContainer").innerHTML = `
		<div class="col-md-12">
			<section class="jumbotron my-5 bg-white">
				<h5 class="text-center">First Name: <b style="text-transform: uppercase;">${jsonData.firstName}</b> | Last Name: <b style="text-transform: uppercase;">${jsonData.lastName}</b> | Email: <b style="text-transform: uppercase;">${jsonData.email}</b> | Mobile No: <b style="text-transform: uppercase;">${jsonData.mobileNo}</b></h5>
				<table class="table text-center">
				<tr>
					<th>Course Name </th>
					<th>Description </th>
					<th>Enrolled On </th>
					<th>Status </th>
					<tbody>${newHtml}</tbody>
				</tr>
			</table>
			</section>
		</div>
		`;
    });	
	// lets create a checker here to make sure the fetch is successful
	// console.log(jsonData)
	// how are we going to display the information inside the frontend component?
	// lets target the div element first using its id attribute
	// lets create a section to display all the courses the user is enrolled in
	

	} else {
		localStorage.clear()
		window.location.replace('./login.html')
	}
}).catch(err => {
     // do something with an error here
     console.log(err);
});


// fetch("http://wonderful-sky-1993.herokuapp.com/api/users/details", {
//     method: 'GET',
//     headers: {
//         'Content-Type': 'application/json',
//         'Authorization': `Bearer ${token}`
//     }
// }).then(res => res.json())
// .then(jsonData => {
//     if (jsonData) {
    
//     let userSubjectPromises = jsonData.enrollments.map(subject => {
//         return fetch(`http://wonderful-sky-1993.herokuapp.com/api/courses/${subject.courseId}`).then(res => res.json()).then(subjectData => {
//             return (`
//                     <tr>
//                         <td>${subjectData.name}</td>
//                         <td>${subject.enrolledOn}</td>
//                         <td>${subject.status}</td>
//                     </tr>
//                 `
//             )
//         })
//     });

//     return Promise.all(userSubjectPromises).then(results => {
//         const newHtml = results.join("");
//         document.getElementById("profileContainer").innerHTML = `
// 		<div class="col-md-12">
// 			<section class="jumbotron my-5 bg-white">
// 				<h5 class="text-center">First Name: <b style="text-transform: uppercase;">${jsonData.firstName}</b> | Last Name: <b style="text-transform: uppercase;">${jsonData.lastName}</b> | Email: <b style="text-transform: uppercase;">${jsonData.email}</b> | Mobile No: <b style="text-transform: uppercase;">${jsonData.mobileNo}</b></h5>
// 				<table class="table text-center">
// 				<tr>
// 					<th>Course Name </th>
// 					<th>Enrolled On </th>
// 					<th>Status </th>
// 					<tbody>${newHtml}</tbody>
// 				</tr>
// 			</table>
// 			</section>
// 		</div>
// 		`;
//     });
//     	}
// }).catch(err => {
//      // do something with an error here
//      console.log(err);

// });