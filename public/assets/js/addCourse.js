// create out add course page
let formSubmit = document.querySelector('#createCourse');

// lets acquire an event that will be applied in our form component.
// create a sub function inside the method to describe the procedure or the action that will take place upon triggering the event.
formSubmit.addEventListener("submit", (mangyayari) => {
	mangyayari.preventDefault();

	// lets target the values of each component inside the forms
	let name = document.querySelector("#courseName").value
	let cost = document.querySelector("#coursePrice").value
	let desc = document.querySelector("#courseDesc").value

	// lets display to confirm if the value will appear
	// console.log(name)
	// console.log(cost)
	// console.log(desc)

	// send a request to the backend project to process the data for creating a new entry inside our courses collection.

	if (name !== "" && desc !== "" && cost !== "") {

		fetch('https://wonderful-sky-1993.herokuapp.com/api/courses/course-exist', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name
			})
		}).then(res => {
			return res.json()
		}).then(resulta => {
			if (resulta === false) {
				fetch('https://wonderful-sky-1993.herokuapp.com/api/courses/create', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
			// what are the properties of the document that the user needs to fill?
				name: name,
				description: desc,
				price: cost
			})
		// how can we handle the promise object that will be returned once the fecth method event has happened
		}).then(res => {
		// console.log(res)
		// why do we need to convert the response into a json format
		return res.json()
		}).then(info => {
		// lets check what the response looks like once json format has been applied
		// console.log(res)
		// lets create a control structure that will describe the response of the UI to client

			if (info === true) {
				Swal.fire('Perfect!',
	  				'You have successfully created a new course!',
	  				'success')
			} else {
				Swal.fire('Oh no!',
	  				'You are missing something!',
	  				'error')
			}
		})
			} else {
				Swal.fire('Error Message!',
  				"The course you're trying to create has already been created.",
  				'warning')
			}
		})
	} else if(name === "") {
		Swal.fire('Nah!',
  				'You forgot to enter the name!',
  				'warning')
	} else if(cost === "") {
		Swal.fire('Nah!',
  				'You forgot to enter the price!',
  				'warning')
	} else if(desc === "") {
		Swal.fire('Nah!',
  				'You forgot to enter the description!!',
  				'warning')
	} else {
		Swal.fire('Oh no!',
  				'Something went wrong!',
  				'error')
	}
})