// console.log("hello from js")
// Lets target our form component inside our document
	let loginForm = document.querySelector('#loginUser')

	// the login form will be used by the client to insert his/her account to be authenticated by the app
	loginForm.addEventListener("submit", (pagpasok) => {
		pagpasok.preventDefault()

		// lets capture the values of our form components.
		let email = document.querySelector("#userEmail").value
		var emailExist = $("#userEmail")[0]
		let pass = document.querySelector("#password").value

		// lets create a checker to confirm the acquired values
		// console.log(email)
		// console.log(pass)

		// our next task is to validate the data inside the input fields

		if (email == "" || pass == "") {
			alert("Please input your email or password")
		} else {
			// send a request to the desired endpoint.
			fetch("https://wonderful-sky-1993.herokuapp.com/api/users/login", {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: email,
					password: pass
				})
			}).then(res => {
				// we are instantiating a promise (resolve, rejected)
				return res.json()
			}).then(dataConverted => {
				// check if conversion of the response has been successful
				// console.log(dataConverted.accessToken)
				// console.log(dataConverted)
				// save the generated access token inside the local storage property of the browser
				if (dataConverted.accessToken) 
					{
						localStorage.setItem('token', dataConverted.accessToken)
						// alert("successfully generated access token");
						
						// Make sure before redirect user to another location you have to identify the access rights that you will grant for that user.

						// How can we know if the user is an admin or not?
						fetch("https://wonderful-sky-1993.herokuapp.com/api/users/details", {
							headers: {
								// lets pass on the value of our access token.
								'Authorization': `Bearer ${dataConverted.accessToken}`
							}// upon sending this request we are instantiating a promise that can lead to 2 possible outcomes.. what do we need to do to handle the possible outcome states of this promise?
						}).then(res => {
							return res.json()
						}).then(data => {
							emailExist.setCustomValidity("");
							// check if we are able to get the users data
							// console.log(data)
							// fetching the user details/info is a success
							localStorage.setItem("id", data._id)
							localStorage.setItem("isAdmin", data.isAdmin)
							localStorage.setItem("loginStatus", "Online")
							// As developers its up to you if you want to create a checker make sure that all values are saved properly inside the web storage
							// console.log("items are all saved in local storage")
							let isAdmin = localStorage.getItem("isAdmin");
						    if (isAdmin !== "true") {
						      window.location.replace('./profile.html')
						    } else {
						      window.location.replace('./addCourse.html')
						    }
						})

						// how would you redirect the user to another location in your app?
					} else {
						// This block of code will run if no access was generated
						// alert("no access token generated")
						Swal.fire({
							icon: "warning",
							text: "It's either the password or email address is incorrect!"
						})
						// emailExist.setCustomValidity("It's either the email address or the password is incorrect!");
					}
			})
		}

	})