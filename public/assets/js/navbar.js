// console.log("Hello from navbar JS")

// we captured the nav session element from the navbar component
let navItem = document.querySelector("#navSession")

// how will we know if there is a user currently logged in?
let userToken = localStorage.getItem("token")
let isAdminCheck  = localStorage.getItem("isAdmin")

// lets create a control structure that will determine the display inside the navbar if there is a user currently logged in the app.

if (!userToken) {
	navItem.innerHTML = 
	`
		<li class="nav-item">
			<a href="./register.html" class="nav-link">Register</a>
		</li>
		<li class="nav-item">
			<a href="./login.html" class="nav-link">Login</a>
		</li>
	`
} else {
	if (isAdminCheck === "true") {
		navItem.innerHTML = 
	`
		<div class="collapse navbar-collapse" id="zuitterNav">
            <!-- create unlist section to list down features (ul.navbar-nav.ml-auto>(li.navhref="./index.html"].nav-link)*3) -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="./../index.html" class="nav-link">Home</a></li>
                <li class="nav-item">
					<a href="./addCourse.html" class="nav-link">Add Course</a>
				</li>
				<li class="nav-item">
					<a href="./courses.html" class="nav-link">Courses</a>
				</li>
                <li class="nav-item">
					<a href="./logout.html" class="nav-link">Logout</a>
				</li>    
             </ul>
        </div>
	`
	} else {
		navItem.innerHTML = 
	`
		<div class="collapse navbar-collapse" id="zuitterNav">
            <!-- create unlist section to list down features (ul.navbar-nav.ml-auto>(li.navhref="./index.html"].nav-link)*3) -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="./../index.html" class="nav-link">Home</a></li>
                <li class="nav-item">
                    <a href="./profile.html" class="nav-link">My Profile</a></li>
                <li class="nav-item">
                    <a href="./courses.html" class="nav-link">Courses</a></li>
                <li class="nav-item">
					<a href="./logout.html" class="nav-link">Logout</a>
				</li>    
             </ul>
        </div>
	`
	}
}