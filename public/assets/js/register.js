// console.log("Hello from JS file");

// Lets target first our form component and place inside a variable (let, cons, var)
let registerForm = document.querySelector("#registerUser");

// Call the form using event
// Inside the first param of the method describe that it will listen to (connection between front and back end) - in this scenario, we will use the submit button
// Inside the 2nd param, lets describe the action / procedure that will happen upon triggering the said event
registerForm.addEventListener("submit", (pangyayari) => {
	// preventDefault = Stop Refresh or to avoid page refresh or page redirection once that the said event has been triggered.
	pangyayari.preventDefault()

	let strength = localStorage.getItem("strength");
	console.log(strength)
	// capture each values inside the input fields first, then lets repackage them inside the new variable
	let firstName = document.querySelector("#firstName").value
	// lets create a checker to make sure that we are successful in capturing the values.
	//console.log(firstName)
	let lastName = document.querySelector("#lastName").value
	// lets create a checker to make sure that we are successful in capturing the values.
	//console.log(lastName)
	let email = document.querySelector("#userEmail").value
	var emailExist = $("#userEmail")[0]
	// lets create a checker to make sure that we are successful in capturing the values.
	//console.log(email)
	let mobileNo = document.querySelector("#mobileNo").value
	var mobileNoLess = $("#mobileNo")[0]
	// lets create a checker to make sure that we are successful in capturing the values.
	//console.log(mobileNo)
	let password = document.querySelector("#password").value
	// lets create a checker to make sure that we are successful in capturing the values.
	//console.log(password)
	let verifyPassword = document.querySelector("#confirm-password").value
	// lets create a checker to make sure that we are successful in capturing the values.
	//console.log(verifyPassword)

	// lets create a data validation for our register page, why?
	// why do we need to validate data .. to check and verify if the data that we will accept is accurate

	// the following info/data that we can validate
	// email, password, mobileNo
	// lets create a control structure to determine the next set of procedure before the user can register a new account.
	// it will be a lot more efficient for you to sanitize the data before submitting it to the backend/server for processing

	//let mobileStr = mobileNo.substr(0,2)

	// Find a way so that the mobile number will be able to accept both foreign and local mobile number formats.
	if((firstName !== "" && lastName !== "" && email !== "" && password !== "" && verifyPassword !== "") && (password === verifyPassword) && (mobileNo.length === 10) && strength > 3 && (password.length > 7 && password.length < 16))
	{
		// how are we going to integrate our email-exist method
		// we are going to send out a new request
		// before you allow the user to create a new account, check if the email value is still available for use. this will ensure that each use will have their unique user email
		fetch('https://wonderful-sky-1993.herokuapp.com/api/users/email-exist', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			// API only accepts
			body: JSON.stringify({
				email: email
			})			
		}).then(res => {
			return res.json() // to make it readable once the response returns to the client side
		}).then(convertedData => {
			// what would the response look like?
			// lets create a control structure to determine the proper procedure depending on the response
			if (convertedData === false) {
				// lets allow the user to register an account.
				// this block of code will run if the condition is met
		// how can we create a new account for the user using the data that was entered?
		/*
			JS Fetch API
			Provides an interface for fetching resources across the network

			Basic Syntax:
			let promise = fetch(url, [options])

			url = address to be accessed/describes the destination of the request.
			options = optional parameters: methods, headers, etc...
			returns a promise

			Promise states:
			Pending - initial state, neither fulfilled nor rejected
			Fulfilled - operation completed successfully
			Rejected - operation failed
		*/

		// stretch goals: (these are not required)
		// The password should contain both alphanumeric characters and atleast 1 special character

				fetch('https://wonderful-sky-1993.herokuapp.com/api/users/register', {
					// we will now describe the structure of the request for registration
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					// API only accepts request in a string format.
					body: JSON.stringify({
						firstName: firstName, 
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password
					})
				}).then(res => {
					// this code will return the promise
					// console.log(res)
					return res.json()
				}).then(data => {
					// this code will convert promise to json format (the result from the backend)
					// console.log(data)
					//lets create here a control structure to give out a proper response depending on the return from the backend
					if(data === true){
						Swal.fire("New Account Registered Successfully!")
					}else{
						// Inform the user that something went wrong
						Swal.fire("Something went wrong during the procedure!")
					}
				})
			} else {
				Swal.fire('Error message!',
  				'The email already exist!',
  				'warning')
			}
		});
	} else if (mobileNo.length !== 10) {
		Swal.fire('Error message!',
  				'Please enter a valid mobile number!',
  				'warning')
	} else if (strength < 4) {
		Swal.fire('Error message!',
  				'Please create a strong password!',
  				'warning')
		// strength = 0;
	}else{
		Swal.fire(`Something went wrong!`)
	}

	// lets create a sweetalert function
	// alert("Successfully captured data")
	// The fire() will allow you to trigger the popup box
	// Swal.fire("Registered Successfully!")
	/*
	Swal.fire({
		icon: "success",
		text: "Registered Successfully!"
	})
	*/
	// What if I want to create different section of my pop up box
	/*Swal.fire(
		// Describe the structure of the card inside an object
			{
				// key: value,
				icon: "success",
				title: "Yayyyyy",
				text: "Successfully Registered!"
			}
		)*/
});

/*
This line of code is for validation
*/

(function () {
  'use strict'

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.querySelectorAll('.registerUser')

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms)
    .forEach(function (form) {
      form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }

        form.classList.add('was-validated')
      }, false)
    })
})()

