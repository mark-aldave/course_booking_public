// Code By Webdevtrick ( https://webdevtrick.com ) 
$(document).ready(function() {

        $('#password').keyup(function() {
            var password = $('#password').val();
            if (checkStrength(password) == false) {
                $('#sign-up').attr('disabled', true);
            }
        });
        $('#confirm-password').blur(function() {
            if ($('#password').val() !== $('#confirm-password').val()) {
                $('#popover-cpassword').removeClass('hide');
                $('#sign-up').attr('disabled', true);
            } else {
                $('#popover-cpassword').addClass('hide');
            }
        });



        function checkStrength(password) {
            var strength = 0;
            //If password contains both lower and uppercase characters, increase strength value.
            if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
                strength += 1;
                $('.low-upper-case').addClass('text-success');
                $('.low-upper-case i').removeClass('fa-times').addClass('fa-check');
                $('#popover-password-top').addClass('hide');


            } else {
                $('.low-upper-case').removeClass('text-success');
                $('.low-upper-case i').addClass('fa-times').removeClass('fa-check');
                $('#popover-password-top').removeClass('hide');
            }

            //If it has numbers and characters, increase strength value.
            if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) {
                strength += 1;
                $('.one-number').addClass('text-success');
                $('.one-number i').removeClass('fa-times').addClass('fa-check');
                $('#popover-password-top').addClass('hide');

            } else {
                $('.one-number').removeClass('text-success');
                $('.one-number i').addClass('fa-times').removeClass('fa-check');
                $('#popover-password-top').removeClass('hide');
            }

            //If it has one special character, increase strength value.
            if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
                strength += 1;
                $('.one-special-char').addClass('text-success');
                $('.one-special-char i').removeClass('fa-times').addClass('fa-check');
                $('#popover-password-top').addClass('hide');

            } else {
                $('.one-special-char').removeClass('text-success');
                $('.one-special-char i').addClass('fa-times').removeClass('fa-check');
                $('#popover-password-top').removeClass('hide');
            }

            if (password.length > 7 && password.length < 16) {
                strength += 1;
                $('.eight-character').addClass('text-success');
                $('.eight-character i').removeClass('fa-times').addClass('fa-check');
                $('#popover-password-top').addClass('hide');

            } else {
                $('.eight-character').removeClass('text-success');
                $('.eight-character i').addClass('fa-times').removeClass('fa-check');
                $('#popover-password-top').removeClass('hide');
            }

            // If value is less than 2

            if (strength < 2) {
                $('#result').removeClass()
                $('#password-strength').addClass('progress-bar-danger');
                $('#result').addClass('text-danger').text('Very Weak');
                $('#password-strength').css('width', '10%');
                localStorage.setItem("strength", strength)
            } else if (strength == 2) {
                $('#result').addClass('good');
                $('#password-strength').removeClass('progress-bar-danger');
                $('#password-strength').addClass('progress-bar-warning');
                $('#result').addClass('text-warning').text('Weak')
                $('#password-strength').css('width', '60%');
                localStorage.setItem("strength", strength)
                return 'Weak'
            } else if (strength == 4) {
                $('#result').removeClass()
                $('#result').addClass('strong');
                $('#password-strength').removeClass('progress-bar-warning');
                $('#password-strength').addClass('progress-bar-success');
                $('#result').addClass('text-success').text('Strong');
                $('#password-strength').css('width', '100%');
                localStorage.setItem("strength", strength)
                return 'Strong'
            }

        }

    });

// To ensure the following JS code is executed when the DOM is ready to be manipulated by JS
// This line of code is used for checking if confirm password matched the password
$(document).ready(function () {
  $("#password, #confirm-password").on("keyup", function () {
    // store the `newPassword` and `confirmPassword` in two variables
    var newPasswordValue = $("#password").val();
    var confirmPasswordValue = $("#confirm-password").val();
    var confirmPassword_input = $("#confirm-password")[0];

    if (newPasswordValue.length > 0 && confirmPasswordValue.length > 0) {
      if (confirmPasswordValue !== newPasswordValue) {
        // $("#password-does-not-match-text").removeAttr("hidden");
        // $("#submitBtn").attr("disabled", true);
        confirmPassword_input.setCustomValidity("Password do not match.");
      }
      if (confirmPasswordValue === newPasswordValue) {
        // $("#submitBtn").removeAttr("disabled");
        // $("#password-does-not-match-text").attr("hidden", true);
        confirmPassword_input.setCustomValidity("");
      }
    }
  });
});