// console.log("Hello log out!")

// upon logging out the credentials of the user should be removed from the local storage.
localStorage.clear();

// the clear method will wipe out/remove the contents of the local storage.

// upon clearing out the local storage property redirect the user back to the login page.
window.location.replace("./login.html")

// Individual Task:
// -> Apply all the necessary changes in the navbar of each page.
// Stretch Goal:
// -> Create a logic that will disable the profile link navbar component if an admin is logged in.
// The admin should be redirected to the courses page only.