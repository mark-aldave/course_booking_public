// what will be the first task in retrieving all records inside the course collection

let adminControls = document.querySelector('#adminButton');
// target the container from the html document.
let container = document.querySelector('#coursesContainer');
// lets determine if there is a user currently logged in
// lets capture one of the properties that is currently stored in our web storage
const isAdmin = localStorage.getItem("isAdmin")
// lets create a control structure to determine the display in the frontend
if (isAdmin == "false" || !isAdmin) {
	adminControls.innerHTML = null;
} else {
	adminControls.innerHTML = `
	<div class="col-md-2 offset-md-10">
	<a href="./addCourse.html" class="btn btn-block btn-warning">Add Course</a>
	</div>
	`
}

// send a request to retrieve all documents from the courses collection
fetch('http://localhost:4000/api/courses/').then(res => res.json()).then(jsonData => {
	console.log(jsonData) // we only inserted this as a checker

	// lets declare a variable that will display the result in the browser depending on the return.
	let courseData;

	// create a control structure that will determine the value that the variable will hold.
	if (jsonData.length < 1) {
		courseData = "No courses available"
		container.innerHTML = courseData
		// console.log(courseData)
	} else {
		// if the condition given is not met, display the contents of the array inside our page.

		// we will iterate the courses collection and display each course inside the browser.
		courseData = jsonData.map(course => {
			// lets check the make up/structure of each element inside the courses collection.
			console.log(course.id)
			// console.log(eachElementInsideArray._id) // This is get the id property only or selected property.
			// lets use template literals to pass/inject the properties of our object inside each section of a card component

			// lets fix the current behavior of our course page that it can only display the last section of a card component.
			// so far we did not indicate what will be the return of our map()

			// let footer1 = `
			// 			<a href="#" class="btn btn-warning text-white">Delete Course</a>
			// 			`
			// let footer2 = `
			// 			<a href="#" class="btn btn-primary text-white">Update Course</a>
			// 			`
			// let content = 
			// 	`	
			// 	<div class="col-md-6 my-3">
			// 		<div class="card">
			// 			<div class="card-body">
			// 				<h3 class="card-title">Course Name: ${course.name}</h3>
			// 				<p class="card-text text-left">Description: <br>${course.description}</p>
			// 				<p class="card-text text-left">Price: ${course.price}</p>
			// 				<p class="card-text text-left">Created On: ${course.createdOn}</p>
			// 			</div>
			// 			<div class="card-footer">
			// 				<a href="#" class="btn btn-success text-white">View Course Details</a>
							
							
			// 			</div>
			// 		</div>
			// 	</div>
			// 	`

			let cardFooter;

			if (isAdmin == "false" || !isAdmin) {
				return(
				`	
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body">
							<h3 class="card-title">Course Name: ${course.name}</h3>
							<p class="card-text text-left">Description: <br>${course.description}</p>
							<p class="card-text text-left">Price: ${course.price}</p>
							<p class="card-text text-left">Created On: ${course.createdOn}</p>
						</div>
						<div class="card-footer">
							<a href="./course.html?courseId=${course._id}" class="btn btn-success text-white btn-block">View Details</a>
						</div>
					</div>
				</div>
				`
					)
			} else {
				return(
				`	
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body">
							<h3 class="card-title">Course Name: ${course.name}</h3>
							<p class="card-text text-left">Description: <br>${course.description}</p>
							<p class="card-text text-left">Price: ${course.price}</p>
							<p class="card-text text-left">Created On: ${course.createdOn}</p>
						</div>
						<div class="card-footer">
							<a href="./course.html?courseId=${course._id}" class="btn btn-success text-white btn-block">View Details</a>
							<a href="#" class="btn btn-danger text-white btn-block">Delete Course</a>
							<a href="#" class="btn btn-primary text-white btn-block">Update Information</a>
						</div>
					</div>
				</div>
				`
					)
			}
				
				
			// console.log(courseData)
			// think of a better message / for documentation purposes lang
		}).join("") // we will use this join() to create a return of a new string
		// it concatenated all the object inside the array
			container.innerHTML = courseData
	}
})

// container.innerHTML = 
// 	`	
// 	<div class="col-md-6 my-3">
// 		<div class="card">
// 			<div class="card-body">
// 				<h3 class="card-title">Course Name</h3>
// 				<p class="card-text text-left">Description</p>
// 				<p class="card-text text-left">Price</p>
// 				<p class="card-text text-left">Created On</p>
// 			</div>
// 		</div>
// 	</div>
// 	`

