// console.log("Your JS is now connected!")

// Lets identify which course this page needs to display inside the browser.
// so earlier we passed the course id data inside the parameters of the url for us to quickly identify which course to display

// how are we going to get the value passed inside the url parameters? 

// easy. the answer is use a URLSearchParams() => this method/constructor creates and returns a URLSearchParams object.

// window.location -> returns a location object with information about the "current" location of the document.
// .search -> contains the queries string section of the current url.

let urlValues = new URLSearchParams(window.location.search)

// lets check what the structure of this variable will look like
// console.log(urlValues) // checker only

// lets get only the desired data from the object using a get method.
let id = urlValues.get('courseId')
console.log(id) // checker only

// lets capture the value of the access token inside the local storage
let token = localStorage.getItem('token')
let userId = localStorage.getItem('id')
console.log(token)

// lets set first a container for all the details that we would want to display.
let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let desc = document.querySelector("#courseDesc")
let enroll = document.querySelector("#enrollmentContainer")

// lets send a request going to the endpoint that will allow us to get and display the course details.
fetch(`http://localhost:4000/api/courses/${id}`).then(res => res.json()).then(convertedData => {
	// console.log(convertedData)
	name.innerHTML = convertedData.name 
	price.innerHTML = convertedData.price 
	desc.innerHTML = convertedData.description 
	enroll.innerHTML = `
		<a id="enrollButton" class="btn btn-success text-white btn-block">Enroll</a>
	`

	document.querySelector("#enrollButton").addEventListener("click", () => {
    

      // insert the course inside the enrollments array of the use
      fetch('http://localhost:4000/api/users/enroll', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`//we have to get the actual value of the token value
        },
        body: JSON.stringify({
          courseId: id
        })
      }).then(res => {
        return res.json()
      }).then(convertedResponse => {
        console.log(convertedResponse)
        // lets create a control structure that will determine a response according to the result that will displayed to the client.
        if (convertedResponse === true) {
          Swal.fire('Congratulations!',
          `You have successfully enrolled to ${convertedData.name}!`,
          'success')
          // we can redirect the user back to the course page
          window.location.replace("./courses.html")
        } else {
          // inform the user if the enrollment failed
          Swal.fire('Error!',
          `Undefined!`,
          'error')
        }
      })
	})
})
