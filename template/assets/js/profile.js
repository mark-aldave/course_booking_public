// console.log("Hello from JS!")
// get the value of the access token inside the local storage and place it inside a new variable

let token = localStorage.getItem("token");
// console.log(token)

let lalagyan = document.querySelector('#profileContainer');
// our goal here is to display the information about the user details
// send a request to the back end project
fetch("http://localhost:4000/api/users/details", {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
}).then(res => res.json())
.then(jsonData => {
	if (jsonData) {
		// lets create a checker here to make sure the fetch is successful
	// console.log(jsonData)

	// get all the elements inside the enrollment array
	console.log(jsonData.enrollments)
let userSubjects = jsonData.enrollments.map(subject => {
		console.log(subject)
		return (
				// I want o create
				`
					<tr>
						<td>${subject.courseId}</td>
						<td> </td>
						<td></td>
					</tr>
				`
			)
	})

	// how are we going to display the information inside the frontend component?
	// lets target the div element first using its id attribute
	// lets create a section to display all the courses the user is enrolled in
	lalagyan.innerHTML = `
		<div class="col-md-12">
			<section class="jumbotron my-5 bg-white">
				<h5 class="text-center">First Name: <b style="text-transform: uppercase;">${jsonData.firstName}</b> | Last Name: <b style="text-transform: uppercase;">${jsonData.lastName}</b> | Email: <b style="text-transform: uppercase;">${jsonData.email}</b> | Mobile No: <b style="text-transform: uppercase;">${jsonData.mobileNo}</b></h5>
				<table class="table text-center">
				<tr>
					<th>Course ID </th>
					<th>Enrolled On </th>
					<th>Status </th>
					<tbody>${userSubjects}</tbody>
				</tr>
			</table>
			</section>
		</div>
	`
	} else {
		localStorage.clear()
		window.location.replace('./login.html')
	}
})